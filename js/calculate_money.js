  // caculate amount of money for all element is default 
  document.getElementById("money_total").innerHTML = 50 - checkRef(50, 'Yes') - checkAge(50, 16);

  var member_totals = document.getElementById("member_totals");

  // set init value and value when change scroll to element [value] in the right
  function setRangeValue(i) {
     var range_slider = document.getElementById("range_slider_" + i);
     var age_value = document.getElementById("range_value_" + i);
     // set init range element [value] is default value  = 16
     age_value.innerHTML = range_slider.value;
     // set range element [value] when change scroll
     range_slider.oninput = function () {
        age_value.innerHTML = range_slider.value;
     }
  }

  function checkAge(cost, age) {
     var paymentViaAge = 0;
     if (16 <= age && age <= 19) {
        paymentViaAge = cost / 100 * 10
     } else {
        paymentViaAge = 0;
     }
     return paymentViaAge;
  }

  function checkRef(cost, ref_type) {
     var paymentViaRef = 0;
     if (ref_type == 'Yes') {
        paymentViaRef = cost / 100 * 5
     } else {
        paymentViaRef = 0;
     }
     return paymentViaRef;
  }

  function discount2_5() {
     return 40 / 100 * 2.5;
  }


  //event onchang when change value selectbox MembesShip Type 
  function onChnageMeberType() {
  }
  //event onchang when change value selectbox Dualration Of Membership  
  function onChnageMeberDuration() { }

  function mountTotals() {
     var add_member = document.getElementById("add_member");
     var member_type = document.getElementById("member_type").value;
     var member_dualration = member_duaration = document.getElementById("member_duaration").value;
     var ref_type = document.querySelector('input[name="referral"]:checked').value;
     var age = document.getElementById('range_slider_1').value;
     console.log(member_type + '---' + member_dualration + '---' + ref_type + '---' + age);
     var payment_money = 0;

     if (member_type == "Family") {
        var member_count = parseInt(member_totals.innerText);
        if (member_count == 2) {
           payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age)) * member_dualration + (40 - checkAge(40, document.getElementById('range_slider_2').value)) * member_dualration;
        } else if (2 < member_count <= 5) {
           //caculate money for the first member
           payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age) - discount2_5()) * member_dualration
           //caculate money for the member 2 to 5
           for (let i = 2; i <= member_count; i++) {
              payment_money += (40 - checkAge(40, document.getElementById("range_slider_" + i + "").value) - discount2_5()) * member_dualration;
           }
        }
        if (member_count > 5) {
           //caculate money for the first member
           payment_money = (40 - checkRef(40, ref_type) - checkAge(40, age) - discount2_5()) * member_dualration
           //caculate money for the member 2 to 5
           for (let i = 2; i <= 5; i++) {
              payment_money += (40 - checkAge(40, document.getElementById("range_slider_" + i + "").value) - discount2_5()) * member_dualration;
           }
           //caculate money for the member 6 to up
           for (let i = 6; i <= member_count; i++) {
              payment_money += 40;
           }
        }
        //display [add member] button 
        add_member.style.display = "block";
        // displays child member container 
        document.getElementById('member_child').style.display = 'block';
     } else {
        //hide [add member] button 
        add_member.style.display = "none";
        // hide child member container 
        document.getElementById('member_child').style.display = 'none';

        payment_money = (50 - checkRef(50, ref_type) - checkAge(50, age)) * member_dualration;
     }

     document.getElementById("money_total").innerHTML = payment_money;
  }



  // event onclick when  click to button [add member]
  function OnlickAddButton() {
     var i = parseInt(member_totals.innerText) + 1;
     member_totals.innerHTML = i;
     // append add member elements when click to button [add member] 
     document.querySelector('#member_child').insertAdjacentHTML('afterbegin', "<div class='sub_member' id='member_" + i + "'> <div class='form-group '> <div class='form-wrapper'> <label for=''>First Name </label> <input name='first_name' id='first_name_" + i + "' type='text' class='form-control h-30'> </div> <div class='form-wrapper'> <label for=''>Second Name </label> <input name='second_name' id='second_name_" + i + "' type='text' class='form-control h-30'> </div> </div> <div class='form-group '> <div class='form-wrapper w-10'> <label for=''>Age</label> </div> <div class='form-wrapper w-70'> <input name='age' type='range' min='0' max='100' value='16' class='myslider w-95' oninput='setRangeValue(" + i + ")' id='range_slider_" + i + "'> </div> <div class='form-wrapper w-20'> <span>Value:</span> <span id='range_value_" + i + "'>16</span> </div> </div> <input type='button' class='remove' id='remove_" + i + "' value='x' onclick='removeRow(this)'> </div>")
     // hides previous child add member elements
     for (let index = 2; index <= i; index++) {
        var remove_button_hidden = document.getElementById("remove_" + index + "");
        remove_button_hidden.style.display = "none";
     }
     // displays [x] on the last added child member
     show_x_button(parseInt(member_totals.innerText))
     mountTotals()
  }

  // remove submember member when click button [x]
  function removeRow(input) {
     var i = parseInt(member_totals.innerText) - 1;
     member_totals.innerHTML = i;
     input.parentNode.remove();
     // displays [x] on the last added child member
     if (i > 2) {
        show_x_button(i)
     }
  }


  function show_x_button(index) {
     var remove_button_show = document.getElementById("remove_" + index + "");
     remove_button_show.style.display = "block";
  }

  // validate form after submit
  function checkValidate() {
     var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
     // var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
     var email = document.getElementById("email");
     var child_container = document.getElementById('member_child');
     // if child container hide only check validate for member_1  =>  i = 1
     var i = child_container.style.display == 'none' ? 1 : parseInt(member_totals.innerText);

     for (let index = 1; index <= i; index++) {
        var age = document.getElementById("range_slider_" + index);
        var first_name = document.getElementById("first_name_" + index);
        var second_name = document.getElementById("second_name_" + index);
        if (first_name.value.length == 0) {
           first_name.setCustomValidity("Please input your first name.");
        } else {
           first_name.setCustomValidity('');
        }
        if (second_name.value.length == 0) {
           second_name.setCustomValidity("Please input your second name.");
        } else {
           second_name.setCustomValidity('');
        }
        if (age.value < 16) {
           age.setCustomValidity('Please input age 16 and up.');
        } else {
           age.setCustomValidity('');
        }
     }
     if (email.value.length == 0) {
        email.setCustomValidity("Please input your e-maill.");
     } else if (!email.value.match(pattern)) {
        email.setCustomValidity('You have entered an invalid e-mail address.');
     } else {
        email.setCustomValidity('');
     }
  }
  document.getElementsByName("submit")[0].onclick = checkValidate;